package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.StudyingPlan;
import com.example.kursovalabkaa.model.Subject;
import com.example.kursovalabkaa.repository.SubjectRepository;
import com.example.kursovalabkaa.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SubjectRestController {
    @Autowired
    SubjectService subjectService;
@Autowired
    SubjectRepository subjectRepository;
    @CrossOrigin(origins = "*")
    @RequestMapping("/api/subject/list")
    public List<Subject> showAll() {
        return subjectService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/subject/create", method = RequestMethod.POST)
    public void create(@RequestBody Subject subject) {
        subjectService.create(subject);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/subject/update", method = RequestMethod.POST)
    public void update(@RequestBody Subject subject) {
        subjectService.update(subject);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/subject/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        subjectService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/subject/getOne/{id}", method = RequestMethod.GET)
    public Subject getOneSubject(@PathVariable String id) {
        return subjectRepository.findById(id).orElse(null);
    }
}
