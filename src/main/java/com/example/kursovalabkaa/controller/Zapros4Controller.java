package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.*;
import com.example.kursovalabkaa.service.TeacherService;
import com.example.kursovalabkaa.service.Zapros4Service;
import com.example.kursovalabkaa.service.Zapros9Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Zapros4Controller {
    @Autowired
    TeacherService teacherService;
    @Autowired
    Zapros4Service zapros4Service;
    public List<Zapros4Out> answear;
    public List<Zapros3Out> answear3;
    public List<Zapros5Out> answear5;
    public List<Zapros5Out> answear6;
    public List<Zapros9Out> answear9;
    public List<Zapros7Out> answear7;
    public List<Zapros7Out> answear8;
    public List<Zapros13Out> answear13;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/makeQuery4", method = RequestMethod.POST)
    public void findByParams(@RequestBody Zapros4In data) {
        answear = zapros4Service.findByParams(data);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getQuery4", method = RequestMethod.GET)
    public List<Zapros4Out> getFindByParams() {
        return answear;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/makeQuery3", method = RequestMethod.POST)
    public void findByParams3(@RequestBody Zapros3In data) {
        answear3 = zapros4Service.query3(data);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getQuery3", method = RequestMethod.GET)
    public List<Zapros3Out> getFindByParams3() {
        return answear3;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/makeQuery5", method = RequestMethod.POST)
    public void findByParams5(@RequestBody Zapros5In data) {
        answear5 = zapros4Service.query5(data);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getQuery5", method = RequestMethod.GET)
    public List<Zapros5Out> getFindByParams5() {
        return answear5;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/makeQuery6", method = RequestMethod.POST)
    public void findByParams6(@RequestBody Zapros5In data) {
        answear6 = zapros4Service.query6(data);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getQuery6", method = RequestMethod.GET)
    public List<Zapros5Out> getFindByParams6() {
        return answear6;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/makeQuery7", method = RequestMethod.POST)
    public void findByParams7(@RequestBody Zapros7In data) {
        answear7 = zapros4Service.query7(data);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getQuery7", method = RequestMethod.GET)
    public List<Zapros7Out> getFindByParams7() {
        return answear7;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/makeQuery8", method = RequestMethod.POST)
    public void findByParams8(@RequestBody Zapros7In data) {
        answear8 = zapros4Service.query8(data);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getQuery8", method = RequestMethod.GET)
    public List<Zapros7Out> getFindByParams8() {
        return answear8;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/makeQuery13", method = RequestMethod.POST)
    public void findByParams13(@RequestBody Zapros13 data) {
        answear13 = teacherService.zapros13(data);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getQuery13", method = RequestMethod.GET)
    public List<Zapros13Out> getFindByParams13() {
        return answear13;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/makeQuery9", method = RequestMethod.POST)
    public void findByParams9(@RequestBody Zapros9In data) {
        System.out.println(data.getGrupa());
        answear9 = zapros4Service.query9(data);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getQuery9", method = RequestMethod.GET)
    public List<Zapros9Out> getFindByParams9() {
        return answear9;
    }


}
