package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.SpecialityTeacher;
import com.example.kursovalabkaa.repository.SpecialityTeacherRepository;
import com.example.kursovalabkaa.service.SpecialityTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SpecialityTeacherRestController {

    @Autowired
    SpecialityTeacherService specialityTeacherService;
    @Autowired
    SpecialityTeacherRepository specialityTeacherRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/speciality/list")
    public List<SpecialityTeacher> showAll() {
        return specialityTeacherService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/speciality/create", method = RequestMethod.POST)
    public void create(@RequestBody SpecialityTeacher specialityTeacher) {
        specialityTeacherService.create(specialityTeacher);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/speciality/update", method = RequestMethod.POST)
    public void update(@RequestBody SpecialityTeacher specialityTeacher) {
        specialityTeacherService.update(specialityTeacher);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/speciality/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        specialityTeacherService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/speciality/getOne/{id}", method = RequestMethod.GET)
    public SpecialityTeacher getOneSpecialityTeacher(@PathVariable String id) {
        return specialityTeacherRepository.findById(id).orElse(null);
    }
}
