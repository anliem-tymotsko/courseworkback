package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.DiplomaQuery;
import com.example.kursovalabkaa.model.Zapros11Out;
import com.example.kursovalabkaa.service.Zapros11Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Zapros11Controller {
    @Autowired
    Zapros11Service zapros11Service;

    public List<Zapros11Out> answear11;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/makeQuery11", method = RequestMethod.POST)
    public void findByParams(@RequestBody DiplomaQuery data) {
        answear11 = zapros11Service.foundByParams(data);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getQuery11", method = RequestMethod.GET)
    public List<Zapros11Out> getFindByParams() {
        return answear11;
    }
}
