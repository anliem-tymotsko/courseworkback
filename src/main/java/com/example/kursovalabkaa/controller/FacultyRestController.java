package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.Faculty;
import com.example.kursovalabkaa.repository.FacultyRepository;
import com.example.kursovalabkaa.service.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FacultyRestController {
    @Autowired
    FacultyService facultyService;
    @Autowired
    FacultyRepository facultyRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/faculty/list")
    public List<Faculty> showAll() {
        return facultyService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/faculty/create", method = RequestMethod.POST)
    public  ResponseEntity create(@RequestBody Faculty faculty) {
        facultyService.create(faculty);
        return new ResponseEntity<Faculty>(faculty, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/faculty/update", method = RequestMethod.POST)
    public void update(@RequestBody Faculty faculty) {
        facultyService.update(faculty);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/faculty/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        facultyService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/faculty/getOne/{id}", method = RequestMethod.GET)
    public Faculty getOneFaculty(@PathVariable String id) {
        return facultyRepository.findById(id).orElse(null);
    }
}
