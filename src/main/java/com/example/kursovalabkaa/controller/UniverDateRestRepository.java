package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.UniverDate;
import com.example.kursovalabkaa.repository.UniverDateRepository;
import com.example.kursovalabkaa.service.UniverDateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UniverDateRestRepository {
    @Autowired
    UniverDateService univerDateService;
    @Autowired
    UniverDateRepository univerDateRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/univerDate/list")
    public List<UniverDate> showAll() {
        return univerDateService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/univerDate/create", method = RequestMethod.POST)
    public void create(@RequestBody UniverDate univerDate) {
        univerDateService.create(univerDate);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/univerDate/update", method = RequestMethod.POST)
    public void update(@RequestBody UniverDate univerDate) {
        univerDateService.update(univerDate);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/univerDate/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        univerDateService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/univerDate/getOne/{id}", method = RequestMethod.GET)
    public UniverDate getOneUniverDate(@PathVariable String id) {
        return univerDateRepository.findById(id).orElse(null);
    }
}
