package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.Grupa;
import com.example.kursovalabkaa.repository.GroupRepository;
import com.example.kursovalabkaa.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GroupRestController {
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    GroupService groupService;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/group/list", method = RequestMethod.GET)
    public List<Grupa> showAll() {
        return groupRepository.findAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/group/create", method = RequestMethod.POST)
    public ResponseEntity create3(@RequestBody Grupa grupa) {
        groupService.create(grupa);
        return new ResponseEntity<Grupa>(grupa, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/group/update", method = RequestMethod.POST)
    public void update(@RequestBody Grupa grupa) {
        groupService.update(grupa);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/group/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        groupService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/group/getOne/{id}", method = RequestMethod.GET)
    public Grupa getOneGroup(@PathVariable String id) {
        return groupService.getOneGroup(id);
    }
}
