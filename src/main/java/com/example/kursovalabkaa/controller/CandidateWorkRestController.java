package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.CandidateWork;
import com.example.kursovalabkaa.repository.CandidateWorkRepository;
import com.example.kursovalabkaa.service.CandidateWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CandidateWorkRestController {
    @Autowired
    CandidateWorkService candidateWorkService;
    @Autowired
    CandidateWorkRepository candidateWorkRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/candidateWork/list")
    public List<CandidateWork> showAll() {
        return candidateWorkService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/candidateWork/create", method = RequestMethod.POST)
    public void create(@RequestBody CandidateWork candidateWork) {
        candidateWorkService.create(candidateWork);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/candidateWork/update", method = RequestMethod.POST)
    public void update(@RequestBody CandidateWork candidateWork) {
        candidateWorkService.update(candidateWork);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/candidateWork/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        candidateWorkService.deleteById(id);
    }
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/candidateWork/getOne/{id}", method = RequestMethod.GET)
    public CandidateWork getOneCandidateWork(@PathVariable String id){  return candidateWorkService.getOneCandidateWork(id);}}

