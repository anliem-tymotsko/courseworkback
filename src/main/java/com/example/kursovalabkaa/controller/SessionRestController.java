package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.Session;
import com.example.kursovalabkaa.repository.SessionRepository;
import com.example.kursovalabkaa.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SessionRestController {
    @Autowired
    SessionService sessionService;
    @Autowired
    SessionRepository sessionRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/session/list")
    public List<Session> showAll() {
        return sessionService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/session/create", method = RequestMethod.POST)
    public void create(@RequestBody Session session) {
        sessionService.create(session);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/session/update", method = RequestMethod.POST)
    public void update(@RequestBody Session session) {
        sessionService.update(session);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/session/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        sessionService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/session/getOne/{id}", method = RequestMethod.GET)
    public Session getOneSession(@PathVariable String id) {
        return sessionRepository.findById(id).orElse(null);
    }
}
