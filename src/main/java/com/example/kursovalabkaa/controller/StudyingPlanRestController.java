package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.StudyingPlan;
import com.example.kursovalabkaa.repository.StudyingPlanRepository;
import com.example.kursovalabkaa.service.StudyingPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudyingPlanRestController {
    @Autowired
    StudyingPlanService studyingPlanService;
    @Autowired
    StudyingPlanRepository studyingPlanRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/studyingPlan/list")
    public List<StudyingPlan> showAll() {
        return studyingPlanService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/studyingPlan/create", method = RequestMethod.POST)
    public void create(@RequestBody StudyingPlan studyingPlan) {
        studyingPlanService.create(studyingPlan);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/studyingPlan/update", method = RequestMethod.POST)
    public void update(@RequestBody StudyingPlan studyingPlan) {
        studyingPlanService.update(studyingPlan);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/studyingPlan/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        studyingPlanService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/studyingPlan/getOne/{id}", method = RequestMethod.GET)
    public StudyingPlan getOneStudyingPlan(@PathVariable String id) {
        return studyingPlanRepository.findById(id).orElse(null);
    }
}
