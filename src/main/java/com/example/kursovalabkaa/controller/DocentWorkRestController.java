package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.DocentWork;
import com.example.kursovalabkaa.repository.DocentWorkRepository;
import com.example.kursovalabkaa.service.DocentWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class DocentWorkRestController {
    @Autowired
    DocentWorkService docentWorkService;
    @Autowired
    DocentWorkRepository docentWorkRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/docentWork/list")
    public List<DocentWork> showAll() {
        return docentWorkService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/docentWork/create", method = RequestMethod.POST)
    public void create(@RequestBody DocentWork docentWork) {
        docentWorkService.create(docentWork);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/docentWork/update", method = RequestMethod.POST)
    public void update(@RequestBody DocentWork docentWork) {
        docentWorkService.update(docentWork);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/docentWork/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        docentWorkService.deleteById(id);
    }
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/docentWork/getOne/{id}", method = RequestMethod.GET)
    public DocentWork getOneDocentWork(@PathVariable String id) {
        return docentWorkRepository.findById(id).orElse(null);
    }
}
