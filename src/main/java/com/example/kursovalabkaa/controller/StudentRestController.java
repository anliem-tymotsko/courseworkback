package com.example.kursovalabkaa.controller;


import com.example.kursovalabkaa.model.Student;
import com.example.kursovalabkaa.repository.StudentRepository;
import com.example.kursovalabkaa.service.FoundStudent;
import com.example.kursovalabkaa.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
public class StudentRestController {
    @Autowired
    StudentService studentService;
    @Autowired
    StudentRepository studentRepository;
    List<Student> searchedStudents = new ArrayList<>();
    @CrossOrigin(origins = "*")
    @RequestMapping("/api/student/list")
    public List<Student> showAll() {
        return studentService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/student/create", method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody Student student) {
        studentService.create(student);
        return new ResponseEntity<Student>(student, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/student/update", method = RequestMethod.POST)
    public void update(@RequestBody Student student) {
        studentService.update(student);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/student/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        studentService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/student/getOne/{id}", method = RequestMethod.GET)
    public Student getOneStudent(@PathVariable String id) {
        return studentRepository.findById(id).orElse(null);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/student/findByParams", method = RequestMethod.POST)
    public List<Student> findByParam(@RequestBody FoundStudent foundStudent) {
        searchedStudents = studentService.findByParams(foundStudent);
        return searchedStudents;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getQuery1", method = RequestMethod.GET)
    public List<Student> findByParam() {
    return searchedStudents;
    }

}
