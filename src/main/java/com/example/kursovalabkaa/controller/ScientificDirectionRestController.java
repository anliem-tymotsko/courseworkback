package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.ScientificDirections;
import com.example.kursovalabkaa.repository.ScientificDirectionsRepository;
import com.example.kursovalabkaa.service.ScientificDirectionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ScientificDirectionRestController {
    @Autowired
    ScientificDirectionsService scientificDirectionsService;
    @Autowired
    ScientificDirectionsRepository directionsRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/scientificDirection/list")
    public List<ScientificDirections> showAll() {
        return scientificDirectionsService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/scientificDirection/create", method = RequestMethod.POST)
    public void create(@RequestBody ScientificDirections scientificDirections) {
        scientificDirectionsService.create(scientificDirections);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/scientificDirection/update", method = RequestMethod.POST)
    public void update(@RequestBody ScientificDirections scientificDirections) {
        scientificDirectionsService.update(scientificDirections);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/scientificDirection/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        scientificDirectionsService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/scientificDirection/getOne/{id}", method = RequestMethod.GET)
    public ScientificDirections getOneScientificDirection(@PathVariable String id) {
        return directionsRepository.findById(id).orElse(null);
    }
}
