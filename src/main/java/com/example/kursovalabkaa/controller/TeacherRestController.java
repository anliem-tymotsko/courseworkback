package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.FoundTeacher;
import com.example.kursovalabkaa.model.Teacher;
import com.example.kursovalabkaa.repository.TeacherRepository;
import com.example.kursovalabkaa.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TeacherRestController {
    @Autowired
    TeacherService teacherService;
    @Autowired
    TeacherRepository teacherRepository;
    List<Teacher> searchedTeachers = new ArrayList<>();

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/teacher/list")
    public List<Teacher> showAll() {
        return teacherService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/teacher/create", method = RequestMethod.POST)
    public void create(@RequestBody Teacher teacher) {
        teacherService.create(teacher);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/teacher/update", method = RequestMethod.POST)
    public void update(@RequestBody Teacher teacher) {
        teacherService.update(teacher);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/teacher/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        teacherService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/teacher/getOne/{id}", method = RequestMethod.GET)
    public Teacher getOneTeacher(@PathVariable String id) {
        return teacherRepository.findById(id).orElse(null);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/teacher/findByParams", method = RequestMethod.POST)
    public List<Teacher> findByParam(@RequestBody FoundTeacher foundTeacher) {
        searchedTeachers = teacherService.findByParams(foundTeacher);
        return searchedTeachers;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getQuery2", method = RequestMethod.GET)
    public List<Teacher> findByParam() {
        return searchedTeachers;
    }
}
