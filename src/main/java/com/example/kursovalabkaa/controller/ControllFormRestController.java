package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.ControllForm;
import com.example.kursovalabkaa.repository.ControllFormRepository;
import com.example.kursovalabkaa.service.ControllFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ControllFormRestController {
    @Autowired
    ControllFormService controllFormService;
    @Autowired
    ControllFormRepository controllFormRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/controllForm/list")
    public List<ControllForm> showAll() {
        return controllFormService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/controllForm/create", method = RequestMethod.POST)
    public void create(@RequestBody ControllForm controllForm) {
        controllFormService.create(controllForm);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/controllForm/update", method = RequestMethod.POST)
    public void update(@RequestBody ControllForm controllForm) {
        controllFormService.update(controllForm);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/controllForm/delete", method = RequestMethod.POST)
    public void delete(@RequestBody  String id) {
        controllFormService.deleteById(id);
    }
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/controllForm/getOne/{id}", method = RequestMethod.GET)
    public ControllForm getOneControllForm(@PathVariable String id) {
        return controllFormRepository.findById(id).orElse(null);
    }
}
