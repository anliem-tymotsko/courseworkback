package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.Diploma;
import com.example.kursovalabkaa.repository.DiplomaRepository;
import com.example.kursovalabkaa.service.DiplomaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DiplomaRestController {
    @Autowired
    DiplomaService diplomaService;
    @Autowired
    DiplomaRepository diplomaRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/diploma/list")
    public List<Diploma> showAll() {
        return diplomaService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/diploma/create", method = RequestMethod.POST)
    public void create(@RequestBody Diploma diploma) {
        diplomaService.create(diploma);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/diploma/update", method = RequestMethod.POST)
    public void update(@RequestBody Diploma diploma) {
        diplomaService.update(diploma);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/diploma/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        diplomaService.deleteById(id);
    }
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/diploma/getOne/{id}", method = RequestMethod.GET)
    public Diploma getOneDiploma(@PathVariable String id) {
        return diplomaRepository.findById(id).orElse(null);
    }
}
