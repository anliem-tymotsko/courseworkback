
package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.forms.DekanatForm;
import com.example.kursovalabkaa.model.Dekanat;
import com.example.kursovalabkaa.service.DekanatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class DekanatWebController {
    @Autowired
    DekanatService dekanatService;

    @RequestMapping("/dekanat/list")
    public String showAll(Model model) {
        List<Dekanat> dekanats = dekanatService.getAll();
        model.addAttribute("dekanats", dekanats);
        return "dekanat";
    }

    @RequestMapping("/dekanat/delete/{id}")
    public String delete(Model model, @PathVariable("id") String id) {
        dekanatService.deleteById(id);
        List<Dekanat> dekanats = dekanatService.getAll();
        model.addAttribute("dekanats", dekanats);
        return "dekanat";
    }

    @RequestMapping(value = "/dekanat/create", method = RequestMethod.GET)
    public String dekanatList(Model model) {
        DekanatForm form = new DekanatForm();
        model.addAttribute("dekanatForm", form);
        return "addDekanat";
    }

    @RequestMapping(value = "/dekanat/create", method = RequestMethod.POST)
    public String dekanatList(Model model, @ModelAttribute("addDekanat") DekanatForm dekanatForm) {

        List<Dekanat> dekanats = dekanatService.getAll();
        Dekanat dekanat = new Dekanat(dekanatForm.getName(), dekanatForm.getDekan());

        dekanatService.create(dekanat);
        dekanats = dekanatService.getAll();
        model.addAttribute("dekanats", dekanats);
        return "dekanat";
    }

  /*  @RequestMapping("/dekanat/requestList")
    public String findByParams(Model model) {
        Dekanat dekanat = new Dekanat("iftkn", "");
        List<Dekanat> dekanats = dekanatService.findByParams(dekanat);
        model.addAttribute("findedDekanats", dekanats);
        return "findedDekanat";
    }*/
}
