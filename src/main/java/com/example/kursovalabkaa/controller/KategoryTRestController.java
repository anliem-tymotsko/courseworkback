package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.KategoryT;
import com.example.kursovalabkaa.repository.KategoryTRepository;
import com.example.kursovalabkaa.service.KategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class KategoryTRestController {

    @Autowired
    KategoryService kategoryService;
    @Autowired
    KategoryTRepository kategoryTRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/kategory/list")
    public List<KategoryT> showAll() {
        return kategoryService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/kategory/create", method = RequestMethod.POST)
    public void create(@RequestBody KategoryT kategoryT) {
        kategoryService.create(kategoryT);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/kategory/update", method = RequestMethod.POST)
    public void update(@RequestBody KategoryT kategoryT) {
        kategoryService.update(kategoryT);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/kategory/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        kategoryService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/kategory/getOne/{id}", method = RequestMethod.GET)
    public KategoryT getOneKategory(@PathVariable String id) {
        return kategoryTRepository.findById(id).orElse(null);
    }
}
