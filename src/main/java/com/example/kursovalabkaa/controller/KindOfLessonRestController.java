package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.KindOfLesson;
import com.example.kursovalabkaa.repository.KindOfLessonRepository;
import com.example.kursovalabkaa.service.KindOfLessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class KindOfLessonRestController {
    @Autowired
    KindOfLessonService kindOfLessonService;
    @Autowired
    KindOfLessonRepository lessonRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/kindOfLesson/list")
    public List<KindOfLesson> showAll() {
        return kindOfLessonService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/kindOfLesson/create", method = RequestMethod.POST)
    public void create(@RequestBody KindOfLesson kindOfLesson) {
        kindOfLessonService.create(kindOfLesson);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/kindOfLesson/update", method = RequestMethod.POST)
    public void update(@RequestBody KindOfLesson kindOfLesson) {
        kindOfLessonService.update(kindOfLesson);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/kindOfLesson/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        kindOfLessonService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/kindOfLesson/getOne/{id}", method = RequestMethod.GET)
    public KindOfLesson getOneKindOfLesson(@PathVariable String id) {
        return lessonRepository.findById(id).orElse(null);
    }
}
