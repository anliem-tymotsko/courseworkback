package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.Kafedra;
import com.example.kursovalabkaa.repository.KafedraRepository;
import com.example.kursovalabkaa.service.KafedraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class KafedraRestController {
    @Autowired
    KafedraService kafedraService;
    @Autowired
    KafedraRepository kafedraRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/kafedra/list")
    public List<Kafedra> showAll() {
        return kafedraService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/kafedra/create", method = RequestMethod.POST)
    public void create(@RequestBody Kafedra kafedra) {
        kafedraService.create(kafedra);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/kafedra/update", method = RequestMethod.POST)
    public void update(@RequestBody Kafedra kafedra) {


        kafedraService.update(kafedra);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/kafedra/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        kafedraService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/kafedra/getOne/{id}", method = RequestMethod.GET)
    public Kafedra getOneKafedra(@PathVariable String id) {
        return kafedraRepository.findById(id).orElse(null);
    }
}
