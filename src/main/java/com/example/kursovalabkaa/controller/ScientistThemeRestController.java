package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.ScientistTheme;
import com.example.kursovalabkaa.repository.ScientistThemeRepository;
import com.example.kursovalabkaa.service.ScientiestThemeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ScientistThemeRestController {
    @Autowired
    ScientiestThemeService scientiestThemeService;
    @Autowired
    ScientistThemeRepository scientistThemeRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/scientistTheme/list")
    public List<ScientistTheme> showAll() {
        return scientiestThemeService.getAll();
    }


    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/scientistTheme/create", method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody ScientistTheme scientistTheme) {
        scientiestThemeService.create(scientistTheme);
        return new ResponseEntity<ScientistTheme>(scientistTheme, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/scientistTheme/update", method = RequestMethod.POST)
    public void update(@RequestBody ScientistTheme scientistTheme) {
        scientiestThemeService.update(scientistTheme);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/scientistTheme/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        scientiestThemeService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/scientistTheme/getOne/{id}", method = RequestMethod.GET)
    public ScientistTheme getOneScientistTheme(@PathVariable String id) {
        return scientistThemeRepository.findById(id).orElse(null);
    }
}
