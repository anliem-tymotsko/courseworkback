package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.Dekanat;
import com.example.kursovalabkaa.repository.DekanatRepository;
import com.example.kursovalabkaa.service.DekanatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DekanatRestController {

    @Autowired
    DekanatService dekanatService;
    @Autowired
    DekanatRepository dekanatRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/dekanat/list", method = RequestMethod.GET)
    public List<Dekanat> showAll() {
        return dekanatService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/dekanat/create", method = RequestMethod.POST)
    public void create(@RequestBody Dekanat dekanat) {
        dekanatService.create(dekanat);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/dekanat/update", method = RequestMethod.POST)
    public void update(@RequestBody Dekanat dekanat) {
        dekanatService.update(dekanat);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/dekanat/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        dekanatService.deleteById(id);
    }
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/dekanat/getOne/{id}", method = RequestMethod.GET)
    public Dekanat getOneDekanat(@PathVariable String id) {
        return dekanatRepository.findById(id).orElse(null);
    }
}
