package com.example.kursovalabkaa.controller;

import com.example.kursovalabkaa.model.Occupation;
import com.example.kursovalabkaa.repository.OccupationRepository;
import com.example.kursovalabkaa.service.OccupationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OccupationRestController {
    @Autowired
    OccupationService occupationService;
    @Autowired
    OccupationRepository occupationRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping("/api/occupation/list")
    public List<Occupation> showAll() {
        return occupationService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/occupation/create", method = RequestMethod.POST)
    public void create(@RequestBody Occupation occupation) {
        occupationService.create(occupation);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/occupation/update", method = RequestMethod.POST)
    public void update(@RequestBody Occupation occupation) {
        occupationService.update(occupation);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/occupation/delete", method = RequestMethod.POST)
    public void delete(@RequestBody String id) {
        occupationService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/occupation/getOne/{id}", method = RequestMethod.GET)
    public Occupation getOneOccupation(@PathVariable String id) {
        return occupationRepository.findById(id).orElse(null);
    }
}
