package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.Student;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends MongoRepository<Student, String> {
    void deleteById(String id);

}
