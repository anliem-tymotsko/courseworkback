package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.ScientistTheme;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScientistThemeRepository extends MongoRepository<ScientistTheme, String> {
    void deleteById(String id);
}
