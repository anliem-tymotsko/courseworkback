package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.Grupa;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GroupRepository extends MongoRepository<Grupa, String> {
    void deleteById(String id);
    //Optional<Grupa> findById(String id);
}
