package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.Stypendia;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StypendiaRepository extends MongoRepository<Stypendia, String> {
    void deleteById(String id);
}
