package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.KategoryT;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KategoryTRepository extends MongoRepository<KategoryT, String> {
    void deleteById(String id);
}
