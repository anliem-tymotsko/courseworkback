package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.Subject;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectRepository extends MongoRepository<Subject, String> {
    void deleteById(String id);

}
