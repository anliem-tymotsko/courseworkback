package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.SpecialityTeacher;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpecialityTeacherRepository  extends MongoRepository<SpecialityTeacher, String> {
    void deleteById(String id);
}
