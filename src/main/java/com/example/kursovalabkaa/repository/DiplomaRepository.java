package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.Diploma;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiplomaRepository extends MongoRepository<Diploma, String> {
    void deleteById(String id);

}
