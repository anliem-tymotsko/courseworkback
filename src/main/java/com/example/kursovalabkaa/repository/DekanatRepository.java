package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.Dekanat;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DekanatRepository extends MongoRepository<Dekanat, String> {
    void deleteById(String i);
}