package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.DocentWork;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocentWorkRepository extends MongoRepository<DocentWork, String> {
    void deleteById(String id);
}
