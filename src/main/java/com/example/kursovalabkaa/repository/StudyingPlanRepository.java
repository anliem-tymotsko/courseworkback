package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.StudyingPlan;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudyingPlanRepository extends MongoRepository<StudyingPlan, String > {
    void deleteById(String id);

}
