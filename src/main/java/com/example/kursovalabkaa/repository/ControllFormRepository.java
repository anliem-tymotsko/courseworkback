package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.ControllForm;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ControllFormRepository extends MongoRepository<ControllForm, String> {
    void deleteById(String id);
}
