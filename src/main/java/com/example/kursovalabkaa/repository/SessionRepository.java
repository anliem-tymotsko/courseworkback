package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.Session;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionRepository extends MongoRepository<Session, String> {
    void deleteById(String id);
}
