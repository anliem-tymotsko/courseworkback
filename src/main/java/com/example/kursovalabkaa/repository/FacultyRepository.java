package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.Faculty;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FacultyRepository  extends MongoRepository<Faculty, String> {
    void deleteById(String i);
}
