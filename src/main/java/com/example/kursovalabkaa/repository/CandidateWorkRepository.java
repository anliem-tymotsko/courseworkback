package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.CandidateWork;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidateWorkRepository extends MongoRepository<CandidateWork, String> {
    void deleteById(String id);
}
