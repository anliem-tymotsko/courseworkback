package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.Teacher;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends MongoRepository <Teacher,String>{
    void deleteById(String id);
}
