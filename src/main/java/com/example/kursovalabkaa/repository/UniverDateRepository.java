package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.UniverDate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UniverDateRepository extends MongoRepository<UniverDate, String> {
    void deleteById(String id);
}
