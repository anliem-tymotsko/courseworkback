package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.ScientificDirections;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScientificDirectionsRepository extends MongoRepository<ScientificDirections, String> {
    void deleteById(String id);
}
