package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.Occupation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OccupationRepository extends MongoRepository<Occupation, String> {
    void deleteById(String id);
}
