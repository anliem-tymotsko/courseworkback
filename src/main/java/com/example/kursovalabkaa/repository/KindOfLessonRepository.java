package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.KindOfLesson;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface KindOfLessonRepository extends MongoRepository<KindOfLesson, String> {
    void deleteById (String id);
}
