package com.example.kursovalabkaa.repository;

import com.example.kursovalabkaa.model.Kafedra;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KafedraRepository extends MongoRepository<Kafedra,String> {
    void deleteById(String id);
}
