package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.ScientistTheme;
import com.example.kursovalabkaa.repository.ScientistThemeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ScientiestThemeService {

    @Autowired
    ScientistThemeRepository scientistThemeRepository;

    public List<ScientistTheme> getAll() {
        return scientistThemeRepository.findAll();
    }

    public ScientistTheme get(String id) {
        return scientistThemeRepository.findById(id).orElse(null);
    }

    public ScientistTheme getOneScientistTheme(String id) {
        return scientistThemeRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        scientistThemeRepository.deleteById(id);

    }

    public void create(ScientistTheme scientistTheme) {
        scientistThemeRepository.save(scientistTheme);
    }

    public void update(ScientistTheme scientistTheme) {
        ScientistTheme newTheme = new ScientistTheme();
        newTheme.setId(scientistTheme.getId());
        newTheme.setThemeOfWork(scientistTheme.getThemeOfWork());
        newTheme.setTeacher(scientistTheme.getTeacher());
        newTheme.setDate(scientistTheme.getDate());
        scientistThemeRepository.deleteById(scientistTheme.getId());
        scientistThemeRepository.save(newTheme);
    }
}
