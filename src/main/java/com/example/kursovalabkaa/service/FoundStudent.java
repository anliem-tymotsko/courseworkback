package com.example.kursovalabkaa.service;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class FoundStudent {
    @Id
    String id;
    String idGrupa;
    String studyingYear;
    String fakulty;
    String year;
    String age;
    String children;
    String stypendia;

    public FoundStudent(String id, String idGrupa, String studyingYear, String fakulty, String year, String age, String children, String stypendia) {
        this.id = id;
        this.idGrupa = idGrupa;
        this.studyingYear = studyingYear;
        this.fakulty = fakulty;
        this.year = year;
        this.age = age;
        this.children = children;
        this.stypendia = stypendia;
    }

    public FoundStudent() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdGrupa() {
        return idGrupa;
    }

    public void setIdGrupa(String idGrupa) {
        this.idGrupa = idGrupa;
    }

    public String getStudyingYear() {
        return studyingYear;
    }

    public void setStudyingYear(String studyingYear) {
        this.studyingYear = studyingYear;
    }

    public String getFakulty() {
        return fakulty;
    }

    public void setFakulty(String fakulty) {
        this.fakulty = fakulty;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public String getStypendia() {
        return stypendia;
    }

    public void setStypendia(String stypendia) {
        this.stypendia = stypendia;
    }
}
