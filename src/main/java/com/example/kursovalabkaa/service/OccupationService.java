package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.Occupation;
import com.example.kursovalabkaa.repository.OccupationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OccupationService {
    List<Occupation> occupationList = new ArrayList<>();
    @Autowired
    OccupationRepository occupationRepository;

    public List<Occupation> getAll() {
        return occupationRepository.findAll();
    }
    public Occupation getOneOccupation(String id) {
        return occupationRepository.findById(id).orElse(null);
    }
    public Occupation get(String id) {
        return occupationRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        occupationRepository.deleteById(id);

    }

    public void create(Occupation occupation) {
        occupationRepository.save(occupation);
    }

    public void update(Occupation occupation) {
        Occupation newOccupation = new Occupation();
        newOccupation.setId(occupation.getId());
        newOccupation.setIdStudingPlan(occupation.getIdStudingPlan());
        newOccupation.setNameTeacher(occupation.getNameTeacher());
        occupationRepository.deleteById(occupation.getId());
        occupationRepository.save(newOccupation);
    }
}
