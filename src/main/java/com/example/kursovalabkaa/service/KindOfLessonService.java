package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.KindOfLesson;
import com.example.kursovalabkaa.repository.KindOfLessonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KindOfLessonService {

    @Autowired
    KindOfLessonRepository kindOfLessonRepository;

    public List<KindOfLesson> getAll() {
        return kindOfLessonRepository.findAll();
    }

    public KindOfLesson getOneKindOfLesson(String id) {
        return kindOfLessonRepository.findById(id).orElse(null);
    }

    public KindOfLesson get(String id) {
        return kindOfLessonRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        kindOfLessonRepository.deleteById(id);

    }

    public void create(KindOfLesson kindOfLesson) {
        kindOfLessonRepository.save(kindOfLesson);
    }

    public void update(KindOfLesson kindOfLesson) {
        KindOfLesson newKindOfLesson = new KindOfLesson();
        newKindOfLesson.setId(kindOfLesson.getId());
        newKindOfLesson.setName(kindOfLesson.getName());
        kindOfLessonRepository.deleteById(kindOfLesson.getId());
        kindOfLessonRepository.save(newKindOfLesson);
    }
}
