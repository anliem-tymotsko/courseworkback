package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.Grupa;
import com.example.kursovalabkaa.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GroupService {

    @Autowired
    GroupRepository groupRepository;


    public List<Grupa> getAll() {
        return groupRepository.findAll();
    }

    public Grupa get(String id) {
        return groupRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        groupRepository.deleteById(id);
    }

    public Grupa getOneGroup(String id) {
        return groupRepository.findById(id).orElse(null);
    }

    public void create(Grupa grupa) {
        groupRepository.save(grupa);
    }


    public void update(Grupa grupa) {
        Grupa newGrupa = new Grupa();
        newGrupa.setId(grupa.getId());
        newGrupa.setName(grupa.getName());
        newGrupa.setKafedra(grupa.getKafedra());
        newGrupa.setKurator(grupa.getKurator());
        newGrupa.setYear(grupa.getYear());
        groupRepository.deleteById(grupa.getId());
        groupRepository.save(newGrupa);
    }
}
