package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.Diploma;
import com.example.kursovalabkaa.repository.DiplomaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DiplomaService {
    @Autowired
    DiplomaRepository diplomaRepository;

    List<Diploma> diplomaList = new ArrayList<>();

    public List<Diploma> getAll() {
        return diplomaRepository.findAll();
    }

    public void create(Diploma diploma) {
        diplomaRepository.save(diploma);
    }

    public Diploma getOneDiploma(String id) {
        return diplomaRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        diplomaRepository.deleteById(id);

    }

    public void update(Diploma diploma) {
        Diploma newDiploma = new Diploma();
        newDiploma.setId(diploma.getId());
        newDiploma.setStudent(diploma.getStudent());
        newDiploma.setKerivnyk(diploma.getKerivnyk());
        newDiploma.setTheme(diploma.getTheme());
        diplomaRepository.deleteById(diploma.getId());
        diplomaRepository.save(diploma);
    }
}
