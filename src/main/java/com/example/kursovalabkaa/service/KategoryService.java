package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.KategoryT;
import com.example.kursovalabkaa.repository.KategoryTRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class KategoryService {
    List<KategoryT> kategoryTList = new ArrayList<>();
    @Autowired
    KategoryTRepository kategoryTRepository;

    public List<KategoryT> getAll() {
        return kategoryTRepository.findAll();
    }

    public KategoryT getOneKategoryT(String id) {
        return kategoryTRepository.findById(id).orElse(null);
    }

    public KategoryT get(String id) {
        return kategoryTRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        kategoryTRepository.deleteById(id);

    }

    public void create(KategoryT kategoryT) {
        kategoryTRepository.save(kategoryT);
    }

    public void update(KategoryT kategoryT) {
        KategoryT newKategory = new KategoryT();
        newKategory.setId(kategoryT.getId());
        newKategory.setName(kategoryT.getName());
        kategoryTRepository.deleteById(kategoryT.getId());
        kategoryTRepository.save(newKategory);
    }
}
