package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.Dekanat;
import com.example.kursovalabkaa.repository.DekanatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DekanatService {

    List<Dekanat> dekanats = new ArrayList<>();
    @Autowired
    DekanatRepository dekanatRepository;



    public List<Dekanat> getAll() {
        return dekanatRepository.findAll();
    }

    public Dekanat get(String id) {
        return dekanatRepository.findById(id).orElse(null);
    }

    public Dekanat getOneDekanat(String id) {
        return dekanatRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        dekanatRepository.deleteById(id);
    }

    public void create(Dekanat dekanat) {
        dekanatRepository.save(dekanat);
    }

    public void update(Dekanat dekanat) {
        Dekanat newdekanat = new Dekanat();
        newdekanat.setName(dekanat.getName());
        newdekanat.setDekan(dekanat.getDekan());
        dekanatRepository.deleteById(dekanat.getId());
        dekanats.add(newdekanat);
    }


}
