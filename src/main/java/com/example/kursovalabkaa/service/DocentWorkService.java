package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.DocentWork;
import com.example.kursovalabkaa.model.FoundDocentWork;
import com.example.kursovalabkaa.model.Teacher;
import com.example.kursovalabkaa.repository.DocentWorkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DocentWorkService {
    @Autowired
    DocentWorkRepository docentWorkRepository;
    @Autowired
    TeacherService teacherService;
    List<DocentWork> docentWorkList = new ArrayList<>();

    public List<DocentWork> getAll() {
        return docentWorkRepository.findAll();
    }

    public void create(DocentWork docentWork) {
        docentWorkRepository.save(docentWork);
    }

    public DocentWork getOneDocentWork(String id) {
        return docentWorkRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        docentWorkRepository.deleteById(id);
    }

    public void update(DocentWork docentWork) {
        DocentWork newDocentWork = new DocentWork();
        newDocentWork.setId(docentWork.getId());
        newDocentWork.setThemeOfWork(docentWork.getThemeOfWork());
        newDocentWork.setTeacher(docentWork.getTeacher());
        newDocentWork.setDate(docentWork.getDate());
        docentWorkRepository.deleteById(docentWork.getId());
        docentWorkRepository.save(newDocentWork);
    }



    public List<DocentWork> findByParams(FoundDocentWork foundDocentWork) {
        List<Teacher> teacherList = teacherService.getAll();
        List<DocentWork> docentWorkList = getAll();
        if (!foundDocentWork.getTeacher().equals(""))
            docentWorkList = docentWorkList.stream().filter(d -> d.getTeacher() == foundDocentWork.getTeacher()).collect(Collectors.toList());
        if (!foundDocentWork.getCafedra().equals("")) {
            for (int i = 0; i < teacherList.size(); i++) {
                for (int j = 0; j < docentWorkList.size(); j++)
                    if (teacherList.get(i).getKafedra() != foundDocentWork.getCafedra())
                        docentWorkList.remove(j);
            }
        }
        return docentWorkList;
    }
}
