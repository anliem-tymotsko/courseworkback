package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.Dekanat;
import com.example.kursovalabkaa.model.Faculty;
import com.example.kursovalabkaa.repository.DekanatRepository;
import com.example.kursovalabkaa.repository.FacultyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class FacultyService {

    @Autowired
   FacultyRepository facultyRepository;


    public List<Faculty> getAll() {
        return facultyRepository.findAll();
    }

    public void create(Faculty faculty) {
        facultyRepository.save(faculty);
    }

    public Faculty getOneCandidateWork(String id) {
        return facultyRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        facultyRepository.deleteById(id);

    }

    public void update(Faculty faculty) {
        Faculty newfaculty = new Faculty();
        newfaculty.setId(faculty.getId());
        newfaculty.setName(faculty.getName());
        newfaculty.setKerivnyk(faculty.getKerivnyk());
        newfaculty.setDekanat(faculty.getDekanat());
        facultyRepository.deleteById(faculty.getId());
        facultyRepository.save(newfaculty);
    }

}
