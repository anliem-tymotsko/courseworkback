package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.*;
import com.example.kursovalabkaa.repository.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SessionService {
    List<Session> sessionList = new ArrayList<>();
    @Autowired
    SessionRepository sessionRepository;

    public List<Session> getAll() {
        return sessionRepository.findAll();
    }

    public Session get(String id) {
        return sessionRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        sessionRepository.deleteById(id);

    }

    public void create(Session session) {
        sessionRepository.save(session);
    }

    public void update(Session session) {
        Session newSession = new Session();
        newSession.setId(session.getId());
        newSession.setSubject(session.getSubject());
        newSession.setTeacher(session.getTeacher());
        newSession.setMark(session.getMark());
        newSession.setControllForm(session.getControllForm());
        newSession.setUniverDate(session.getUniverDate());
        newSession.setStudent(session.getStudent());
        sessionRepository.deleteById(session.getId());
        sessionRepository.save(newSession);
    }
}
