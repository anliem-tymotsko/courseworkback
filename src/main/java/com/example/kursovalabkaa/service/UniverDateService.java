package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.UniverDate;
import com.example.kursovalabkaa.repository.UniverDateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UniverDateService {
    List<UniverDate> univerDateList = new ArrayList<>();
    @Autowired
    UniverDateRepository univerDateRepository;

    public List<UniverDate> getAll() {
        return univerDateRepository.findAll();
    }

    public UniverDate get(String id) {
        return univerDateRepository.findById(id).orElse(null);
    }

    public UniverDate getOneUniverDate(String id) {
        return univerDateRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        univerDateRepository.deleteById(id);

    }

    public void create(UniverDate univerDate) {
        univerDateRepository.save(univerDate);
    }

    public void update(UniverDate univerDate) {
        UniverDate newUniverDate = new UniverDate();
        newUniverDate.setId(univerDate.getId());
        newUniverDate.setYear(univerDate.getYear());
        newUniverDate.setSemester(univerDate.getSemester());

        univerDateRepository.deleteById(univerDate.getId());
        univerDateRepository.save(newUniverDate);
    }

}
