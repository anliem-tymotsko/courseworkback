package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.Kafedra;
import com.example.kursovalabkaa.repository.KafedraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class KafedraService {

    @Autowired
    KafedraRepository kafedraRepository;

    public List<Kafedra> getAll() {
        return kafedraRepository.findAll();
    }

    public Kafedra getOneKafedra(String id) {
        return kafedraRepository.findById(id).orElse(null);
    }

    public Kafedra get(String id) {
        return kafedraRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        kafedraRepository.deleteById(id);

    }

    public void create(Kafedra kafedra) {
        kafedraRepository.save(kafedra);
    }

    public void update(Kafedra kafedra) {
        Kafedra newKaf = new Kafedra();
        newKaf.setId(kafedra.getId());
        newKaf.setFaculty(kafedra.getFaculty());
        newKaf.setName(kafedra.getName());
        newKaf.setZavKafedra(kafedra.getZavKafedra());
        kafedraRepository.deleteById(kafedra.getId());
        kafedraRepository.save(newKaf);
    }
}
