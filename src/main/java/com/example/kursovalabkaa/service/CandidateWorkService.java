package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.CandidateWork;
import com.example.kursovalabkaa.model.Teacher;
import com.example.kursovalabkaa.repository.CandidateWorkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CandidateWorkService {
    List<CandidateWork> candidateWorks = new ArrayList<>();
    @Autowired
    CandidateWorkRepository candidateWorkRepository;

    public List<CandidateWork> getAll() {
        return candidateWorkRepository.findAll();
    }
    public CandidateWork getOneCandidateWork(String id) {
        return candidateWorkRepository.findById(id).orElse(null);
    }
    public CandidateWork get(String id) {
        return candidateWorkRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        candidateWorkRepository.deleteById(id);

    }

    public void create(CandidateWork candidateWork) {
      candidateWorkRepository.save(candidateWork);
    }

    public void update(CandidateWork candidateWork) {
        CandidateWork newCandidateWork = new CandidateWork();
        newCandidateWork.setId(candidateWork.getId());
        newCandidateWork.setThemeOfWork(candidateWork.getThemeOfWork());
        newCandidateWork.setTeacher(candidateWork.getTeacher());
        newCandidateWork.setDate(candidateWork.getDate());
        candidateWorkRepository.deleteById(candidateWork.getId());
        candidateWorkRepository.save(newCandidateWork);
    }
}
