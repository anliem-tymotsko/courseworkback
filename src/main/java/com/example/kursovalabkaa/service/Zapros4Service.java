package com.example.kursovalabkaa.service;
import com.example.kursovalabkaa.model.*;
import com.example.kursovalabkaa.repository.GroupRepository;
import com.example.kursovalabkaa.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class Zapros4Service {
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    GroupService groupService;
    @Autowired
    TeacherService teacherService;
    @Autowired
    StudyingPlanService studyingPlanService;
    @Autowired
    OccupationService occupationService;
    @Autowired
    SessionService sessionService;
    @Autowired
    KafedraService kafedraService;
    @Autowired
    StudentService studentService;
    @Autowired
    CandidateWorkService candidateWorkService;
    Zapros4In inputData;
    public List<Zapros4Out> findByParams(Zapros4In data) {
        List<StudyingPlan> allData = studyingPlanService.getAll();
        List<Occupation> allDataOccupation = occupationService.getAll();
        List<Teacher> allDataTeacher = teacherService.getAll();
        List<Occupation> sortedDataOccupation = new ArrayList<>();
        List<Kafedra> allKafedra = kafedraService.getAll();
        allData = allData.stream().filter(g->g.getGrupa().contains(data.getGrupaName())).collect(Collectors.toList());
        allData = allData.stream().filter(g->g.getUniverDate().contains(data.getPeriod())).collect(Collectors.toList());
        List <Zapros4Out> answ = new ArrayList<>();
        for (int i=0; i<allData.size();i++){
            String a = allData.get(i).getOccupation();
            for(int j=0;j<allDataOccupation.size();j++){
                if(allDataOccupation.get(j).getIdStudingPlan() == a){
                    String idTeacher = allDataOccupation.get(j).getNameTeacher();
                    for(int l=0;l<allDataTeacher.size();l++){
                       if(allDataTeacher.get(l).getId() == idTeacher){
                           String kafedraId = allDataTeacher.get(l).getKafedra();
                           for(int k=0;k<allKafedra.size();k++){
                               if(allKafedra.get(k).getId() == kafedraId){
                                   answ.add(new Zapros4Out(allKafedra.get(k).getId(),allKafedra.get(k).getName()));
                               }
                           }
                           }
                    }

                    }
            }
        }
        return answ;
    }
    public List<Zapros3Out> query3(Zapros3In k){
        List<Teacher> allDataTeacher = teacherService.getAll();
        List<CandidateWork> allDataCandidateWork = candidateWorkService.getAll();
        List<String> idTeachers = new ArrayList<>();
        List<Zapros3Out> answ = new ArrayList<>();
        System.out.println(k.getKafedra());
        allDataTeacher = allDataTeacher.stream().filter(g->g.getKafedra().contains(k.getKafedra())).collect(Collectors.toList());
        for (int i=0; i<allDataTeacher.size();i++) {
            idTeachers.add(allDataTeacher.get(i).getId());
            System.out.println(allDataTeacher.get(i).getId());
        }
        System.out.println(allDataTeacher);
        for (int i=0; i<allDataCandidateWork.size();i++) {
            for (int j=0; j<idTeachers.size();j++) {
                System.out.println(allDataCandidateWork);

                if(allDataCandidateWork.get(i).getTeacher() == idTeachers.get(j)){
                    answ.add(new Zapros3Out(allDataCandidateWork.get(i).getId(),allDataCandidateWork.get(i).getThemeOfWork(),allDataCandidateWork.get(i).getTeacher()));
                }
            }
            }
        return answ;
    }
    public List<Zapros5Out> query5(Zapros5In k) {
        List<StudyingPlan> allPlanData = studyingPlanService.getAll();
        allPlanData = allPlanData.stream().filter(g->g.getGrupa().contains(k.getGrupaName())).collect(Collectors.toList());
        allPlanData = allPlanData.stream().filter(g->g.getOccupation().contains(k.getSubjectId())).collect(Collectors.toList());
        List <String> idStudingPlan = new ArrayList<>();
        List <String> idTeachers = new ArrayList<>();
        List<Occupation> allOccupationData = occupationService.getAll();
        List<Teacher> allDataTeacher = teacherService.getAll();

        List<Zapros5Out> answ = new ArrayList<>();

        for (int i=0; i<allPlanData.size();i++) {
           idStudingPlan.add(allPlanData.get(i).getId());
        }
        for(int j=0;j<allOccupationData.size();j++){
            for (int o=0;o<idStudingPlan.size();o++){
                if(allOccupationData.get(j).getIdStudingPlan() == idStudingPlan.get(o)){
                    idTeachers.add(allOccupationData.get(j).getNameTeacher());
                }
            }
        }
        for(int m= 0;m<allDataTeacher.size();m++) {
            for (int l = 0; l < idTeachers.size(); l++) {
                if(allDataTeacher.get(m).getId() == idTeachers.get(l)){
                    answ.add(new Zapros5Out(allDataTeacher.get(m).getId(),allDataTeacher.get(m).getName(),allDataTeacher.get(m).getLastName(),allDataTeacher.get(m).getAfterFathName(),allDataTeacher.get(m).getSpecialityTeacher(),allDataTeacher.get(m).getKategoryT()));
                }
            }
        }
            return answ;
    }
    public List<Zapros5Out> query6(Zapros5In k) {
        List<StudyingPlan> allPlanData = studyingPlanService.getAll();
        allPlanData = allPlanData.stream().filter(g->g.getGrupa().contains(k.getGrupaName())).collect(Collectors.toList());
        allPlanData = allPlanData.stream().filter(g->g.getTypeOccupation().contains(k.getSubjectId())).collect(Collectors.toList());
        List <String> idStudingPlan = new ArrayList<>();
        List <String> idTeachers = new ArrayList<>();
        List<Occupation> allOccupationData = occupationService.getAll();
        List<Teacher> allDataTeacher = teacherService.getAll();

        List<Zapros5Out> answ = new ArrayList<>();

        for (int i=0; i<allPlanData.size();i++) {
            idStudingPlan.add(allPlanData.get(i).getId());
        }
        for(int j=0;j<allOccupationData.size();j++){
            for (int o=0;o<idStudingPlan.size();o++){
                if(allOccupationData.get(j).getIdStudingPlan() == idStudingPlan.get(o)){
                    idTeachers.add(allOccupationData.get(j).getNameTeacher());
                }
            }
        }
        for(int m= 0;m<allDataTeacher.size();m++) {
            for (int l = 0; l < idTeachers.size(); l++) {
                if(allDataTeacher.get(m).getId() == idTeachers.get(l)){
                    answ.add(new Zapros5Out(allDataTeacher.get(m).getId(),allDataTeacher.get(m).getName(),allDataTeacher.get(m).getLastName(),allDataTeacher.get(m).getAfterFathName(),allDataTeacher.get(m).getSpecialityTeacher(),allDataTeacher.get(m).getKategoryT()));
                }
            }
        }
        return answ;
    }
    public List<Zapros7Out> query7(Zapros7In data) {
        List<Zapros7Out> answ = new ArrayList<>();
        List<Session> allSessionData = sessionService.getAll();
        List<Student> allStudentData = studentService.getAll();
        List<Student> sortedStudentData = new ArrayList<>();
        allSessionData = allSessionData.stream().filter(g->g.getMark().contains(data.getMark())).collect(Collectors.toList());
        allSessionData = allSessionData.stream().filter(g->g.getSubject().contains(data.getSubject())).collect(Collectors.toList());
        List <String> idStudents = new ArrayList<>();
        for (int i=0; i<allSessionData.size();i++) {
            idStudents.add(allSessionData.get(i).getStudent());
        }

        for(int m= 0;m<allStudentData.size();m++) {
            for (int l = 0; l < idStudents.size(); l++) {

                if(allStudentData.get(m).getId() == idStudents.get(l)){
                    System.out.println(allStudentData.get(m).getGrupa());
                    System.out.println(data.getGrupa());
                    if(allStudentData.get(m).getGrupa().equals(data.getGrupa())){
                        sortedStudentData.add(allStudentData.get(m));
                        System.out.println("dddddddddddd");
                    }
                }
            }
        }
        for (int i=0; i<sortedStudentData.size();i++) {
            answ.add(new Zapros7Out(sortedStudentData.get(i).getId(),sortedStudentData.get(i).getName(),sortedStudentData.get(i).getLastName(),sortedStudentData.get(i).getAfterFathName(),sortedStudentData.get(i).getDateOfBirth()));
        }
        return answ;
    }

    public List<Zapros7Out> query8(Zapros7In data) {
        List<Zapros7Out> answ = new ArrayList<>();
        List<Session> allSessionData = sessionService.getAll();
        List<Session> sortedSessionData = new ArrayList<>();
        List<Student> allStudentData = studentService.getAll();
        List<Student> sortedStudentData = new ArrayList<>();
        List <String> idStudents = new ArrayList<>();
        for (int i=0; i<allSessionData.size();i++) {
            if(allSessionData.get(i).getMark().contains("4") || allSessionData.get(i).getMark().contains("5")){
                sortedSessionData.add(allSessionData.get(i));
            }
        }
        for (int i=0; i<sortedSessionData.size();i++) {
            idStudents.add(sortedSessionData.get(i).getStudent());
        }
        for(int m= 0;m<allStudentData.size();m++) {
            for (int l = 0; l < idStudents.size(); l++) {

                if(allStudentData.get(m).getId() == idStudents.get(l)){
                    System.out.println(allStudentData.get(m).getGrupa());
                    System.out.println(data.getGrupa());
                    if(allStudentData.get(m).getGrupa().equals(data.getGrupa())){
                        sortedStudentData.add(allStudentData.get(m));
                    }
                }
            }
        }
        for (int i=0; i<sortedStudentData.size();i++) {
            answ.add(new Zapros7Out(sortedStudentData.get(i).getId(),sortedStudentData.get(i).getName(),sortedStudentData.get(i).getLastName(),sortedStudentData.get(i).getAfterFathName(),sortedStudentData.get(i).getDateOfBirth()));
        }
        return answ;
    }
    public List<Zapros9Out> query9(Zapros9In k){
        List<Session> sessions = sessionService.getAll();
        List<Session> sortedSessions = new ArrayList<>();
        List<Zapros9Out> answ = new ArrayList<>();
        sessions = sessions.stream().filter(g -> g.getSubject().contains(k.getSubject())).collect(Collectors.toList());
        sessions = sessions.stream().filter(g -> g.getUniverDate().contains(k.getSemestr())).collect(Collectors.toList());
        for(int i=0; i<sessions.size();i++) {
            if((studentService.getOneStudent(sessions.get(i).getStudent()).getGrupa()).equals(k.getGrupa())){
                sortedSessions.add(sessions.get(i));
            }
        }
            for(int i=0; i<sortedSessions.size();i++){
            answ.add(new Zapros9Out(
                    teacherService.get(sortedSessions.get(i).getTeacher()).getId(),
                    teacherService.get(sortedSessions.get(i).getTeacher()).getName(),
                    teacherService.get(sortedSessions.get(i).getTeacher()).getLastName(),
                    teacherService.get(sortedSessions.get(i).getTeacher()).getAfterFathName(),
                    teacherService.get(sortedSessions.get(i).getTeacher()).getSpecialityTeacher(),
                    teacherService.get(sortedSessions.get(i).getTeacher()).getKategoryT(),
                    teacherService.get(sortedSessions.get(i).getTeacher()).getSalary(),
                    kafedraService.get(teacherService.get(sortedSessions.get(i).getTeacher()).getKafedra()).getName()
            ));
        }
        return answ;
    }


}
