package com.example.kursovalabkaa.service;
import com.example.kursovalabkaa.model.*;
import com.example.kursovalabkaa.repository.GroupRepository;
import com.example.kursovalabkaa.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class Zapros9Service {
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    GroupService groupService;
    @Autowired
    StudentService studentService;
    @Autowired
    TeacherService teacherService;
    @Autowired
    KafedraService kafedraService;
    @Autowired
    SessionService sessionService;
    public List<Zapros9Out> query9(Zapros9In k){
        List<Session> sessions = sessionService.getAll();
        List<String> sortedSessions = new ArrayList<>();
        List<Zapros9Out> answ = new ArrayList<>();
        sessions = sessions.stream().filter(g -> g.getSubject().equals(k.getSubject())).collect(Collectors.toList());
        sessions = sessions.stream().filter(g -> g.getUniverDate().equals(k.getSemestr())).collect(Collectors.toList());
        sessions = sessions.stream().filter(g -> studentService.get(g.getStudent()).getGrupa().equals(k.getGrupa())).collect(Collectors.toList());
    for(int i=0; i<sessions.size();i++){
        answ.add(new Zapros9Out(
                teacherService.get(sessions.get(i).getTeacher()).getId(),
                teacherService.get(sessions.get(i).getTeacher()).getName(),
                teacherService.get(sessions.get(i).getTeacher()).getLastName(),
                teacherService.get(sessions.get(i).getTeacher()).getAfterFathName(),
                teacherService.get(sessions.get(i).getTeacher()).getSpecialityTeacher(),
                teacherService.get(sessions.get(i).getTeacher()).getKategoryT(),
                teacherService.get(sessions.get(i).getTeacher()).getSalary(),
                kafedraService.get(teacherService.get(sessions.get(i).getTeacher()).getKafedra()).getName()
        ));
    }
    return answ;
    }

}
