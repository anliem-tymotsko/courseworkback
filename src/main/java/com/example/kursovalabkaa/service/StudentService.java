package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.Student;
import com.example.kursovalabkaa.repository.GroupRepository;
import com.example.kursovalabkaa.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentService {


    @Autowired
    StudentRepository studentRepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    GroupService groupService;

    public List<Student> getAll() {
        return studentRepository.findAll();
    }

    public Student get(String id) {
        return studentRepository.findById(id).orElse(null);
    }

    public Student getOneStudent(String id) {
        System.out.println(id);
        return studentRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        studentRepository.deleteById(id);
    }

    public void create(Student student) {
        studentRepository.save(student);
    }

    public void update(Student student) {
        Student newStudent = new Student();
        newStudent.setId(student.getId());
        newStudent.setName(student.getName());
        newStudent.setLastName(student.getLastName());
        newStudent.setAfterFathName(student.getAfterFathName());
        newStudent.setDateOfBirth(student.getDateOfBirth());
        newStudent.setGrupa(student.getGrupa());
        newStudent.setCountOfChildren(student.getCountOfChildren());
        newStudent.setStypendia(student.getStypendia());
        studentRepository.deleteById(student.getId());
        studentRepository.save(newStudent);
    }


    public List<Student> findByParams(FoundStudent foundStudent) {
        List<Student> foundStudents = this.getAll();
        if (!foundStudent.getYear().equals("")) {
            System.out.println("jj");
            foundStudents = foundStudents.stream().filter(g -> g.getDateOfBirth().equals(foundStudent.getYear())).collect(Collectors.toList());
        }
        if (!foundStudent.getChildren().equals("")) {
            System.out.println("kk");
            foundStudents = foundStudents.stream().filter(g -> g.getCountOfChildren().equals(foundStudent.getChildren())).collect(Collectors.toList());
        }
            if (!foundStudent.getStypendia().equals("")) {
            foundStudents = foundStudents.stream().filter(g -> g.getStypendia().equals(foundStudent.getStypendia())).collect(Collectors.toList());
        }
        if (!foundStudent.getIdGrupa().equals("")) {
            System.out.println("ss");
            foundStudents = foundStudents.stream().filter(g -> g.getGrupa().equals(foundStudent.getIdGrupa())).collect(Collectors.toList());
        }
        if(!foundStudent.getAge().equals("")){
            int age = Integer.parseInt(foundStudent.getAge());
            int yearOfBirth = 2019 - age;
            String year = String.valueOf(yearOfBirth);
            foundStudents = foundStudents.stream().filter(g->g.getDateOfBirth().contains(year)).collect(Collectors.toList());}
//2019 - Integer.parseInt(g.getDateOfBirth().substring(7,10))==Integer.parseInt(foundStudent.getAge())

           // foundStudents = foundStudents.stream().filter(g -> g.getDateOfBirth().contains(year) && g.getStypendia() == foundStudent.getStypendia() && g.getCountOfChildren() == foundStudent.getChildren()
              //      && g.getGrupa().equals(foundStudent.getIdGrupa())).collect(Collectors.toList());

    /*  if(!foundStudent.getIdGrupa().equals(""))
          foundStudents = foundStudents.stream().filter(g->g.getGrupa().contains(groupService.getOneGroup(foundStudent.getIdGrupa()).getName())).collect(Collectors.toList());
*/

        return foundStudents;
    }
}