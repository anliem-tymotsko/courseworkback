package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.SpecialityTeacher;
import com.example.kursovalabkaa.repository.SpecialityTeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SpecialityTeacherService {

    List<SpecialityTeacher> specialityTeacherList = new ArrayList<>();
    @Autowired
    SpecialityTeacherRepository teacherRepository;

    public List<SpecialityTeacher> getAll() {
        return teacherRepository.findAll();
    }

    public SpecialityTeacher getOneSpecialityTeacher(String id) {
        return teacherRepository.findById(id).orElse(null);
    }

    public SpecialityTeacher get(String id) {
        return teacherRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        teacherRepository.deleteById(id);

    }

    public void create(SpecialityTeacher specialityTeacher) {
        teacherRepository.save(specialityTeacher);
    }

    public void update(SpecialityTeacher specialityTeacher) {
        SpecialityTeacher newSpeciality = new SpecialityTeacher();
        newSpeciality.setId(specialityTeacher.getId());
        newSpeciality.setName(specialityTeacher.getName());
        teacherRepository.deleteById(specialityTeacher.getId());
        teacherRepository.save(newSpeciality);
    }
}
