package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.StudyingPlan;
import com.example.kursovalabkaa.repository.StudyingPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudyingPlanService {
    List<StudyingPlan> studyingPlanList = new ArrayList<>();
    @Autowired
    StudyingPlanRepository studyingPlanRepository;

    public List<StudyingPlan> getAll() {
        return studyingPlanRepository.findAll();
    }

    public StudyingPlan get(String id) {
        return studyingPlanRepository.findById(id).orElse(null);
    }

    public StudyingPlan getOneStudyingPlan(String id) {
        return studyingPlanRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        studyingPlanRepository.deleteById(id);

    }

    public void create(StudyingPlan studyingPlan) {
        studyingPlanRepository.save(studyingPlan);
    }

    public void update(StudyingPlan studyingPlan) {
        StudyingPlan newStudyingPlan = new StudyingPlan();
        newStudyingPlan.setId(studyingPlan.getId());
        newStudyingPlan.setGrupa(studyingPlan.getGrupa());
        newStudyingPlan.setTypeOccupation(studyingPlan.getTypeOccupation());
        newStudyingPlan.setUniverDate(studyingPlan.getUniverDate());
        newStudyingPlan.setOccupation(studyingPlan.getOccupation());
        newStudyingPlan.setHours(studyingPlan.getHours());
        newStudyingPlan.setControllForm(studyingPlan.getControllForm());
        studyingPlanRepository.save(newStudyingPlan);
    }
}
