package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.Subject;
import com.example.kursovalabkaa.repository.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SubjectService {

    List<Subject> subjectList = new ArrayList<>();
    @Autowired
    SubjectRepository subjectRepository;

    public List<Subject> getAll() {
        return subjectRepository.findAll();
    }

    public Subject get(String id) {
        return subjectRepository.findById(id).orElse(null);
    }

    public Subject getOneSubject(String id) {
        return subjectRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        subjectRepository.deleteById(id);

    }

    public void create(Subject subject) {
        subjectRepository.save(subject);
    }

    public void update(Subject subject) {
        Subject newSubject = new Subject();
        newSubject.setId(subject.getId());
        newSubject.setName(subject.getName());
        subjectRepository.deleteById(subject.getId());
        subjectRepository.save(subject);
    }
}
