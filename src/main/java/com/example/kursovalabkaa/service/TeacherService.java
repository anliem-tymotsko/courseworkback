package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.*;
import com.example.kursovalabkaa.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeacherService {

    @Autowired
    TeacherRepository teacherRepository;
    @Autowired
    OccupationService occupationService;
    @Autowired
    TeacherService teacherService;
    @Autowired
    StudyingPlanService studyingPlanService;
    @Autowired
    UniverDateService univerDateService;
    @Autowired
    SubjectService subjectService;

    public List<Teacher> getAll() {
        return teacherRepository.findAll();
    }

    public Teacher get(String id) {
        return teacherRepository.findById(id).orElse(null);
    }

    public Teacher getOneTeacher(String id) {
        return teacherRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        teacherRepository.deleteById(id);

    }

    public void create(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    public void update(Teacher teacher) {
        Teacher newTeacher = new Teacher();
        newTeacher.setId(teacher.getId());
        newTeacher.setName(teacher.getName());
        newTeacher.setLastName(teacher.getLastName());
        newTeacher.setAfterFathName(teacher.getAfterFathName());
        newTeacher.setDateOfBirth(teacher.getDateOfBirth());
        newTeacher.setSpecialityTeacher(teacher.getSpecialityTeacher());
        newTeacher.setKategoryT(teacher.getKategoryT());
        newTeacher.setTelNum(teacher.getTelNum());
        newTeacher.setCountChildren(teacher.getCountChildren());
        newTeacher.setSalary(teacher.getSalary());
        newTeacher.setKafedra(teacher.getKafedra());

        teacherRepository.deleteById(teacher.getId());
        teacherRepository.save(newTeacher);
    }


    public List<Teacher> findByParams(FoundTeacher foundTeacher) {
        List<Teacher> teacherList = this.getAll();
        if (!foundTeacher.getKafedra().equals("")) {
            teacherList = teacherList.stream().filter(t -> t.getKafedra().equals(foundTeacher.getKafedra())).collect(Collectors.toList());
        }
        if (!foundTeacher.getChildren().equals("")){
            teacherList = teacherList.stream().filter(t -> t.getCountChildren().equals(foundTeacher.getChildren())).collect(Collectors.toList());
        }
        if (!foundTeacher.getCategory().equals("")) {
            teacherList = teacherList.stream().filter(t -> t.getKategoryT().equals(foundTeacher.getCategory())).collect(Collectors.toList());
        }
        if (!foundTeacher.getSalary().equals("")) {
            teacherList = teacherList.stream().filter(t -> t.getSalary().equals(foundTeacher.getSalary())).collect(Collectors.toList());
        }
        if (!foundTeacher.getKafedra().equals("")) {
            teacherList = teacherList.stream().filter(t -> t.getKafedra().equals(foundTeacher.getKafedra())).collect(Collectors.toList());
        }
     /*   if(!foundTeacher.getAge().equals("")){
            int age = Integer.parseInt(foundTeacher.getAge());
            int yearOfBirth = 2019 - age;
            String year = String.valueOf(yearOfBirth);
            teacherList = teacherList.stream().filter(g->g.get().contains(year)).collect(Collectors.toList());}

    }*/
        return teacherList;
    }

    public List<Zapros13Out> zapros13(Zapros13 zapros13) {
        List<Occupation> occupations = occupationService.getAll();
        List<Zapros13Out> zapros13Outs = new ArrayList<>();
        List<StudyingPlan> studyingPlans = studyingPlanService.getAll();
        List <String> idPlansFromStudingPlans = new ArrayList<>();
        List <String> idPlanFromOccupation = new ArrayList<>();
        //sorting plans
        studyingPlans = studyingPlans.stream().filter(g -> g.getUniverDate().equals(zapros13.idStudingYear)).collect(Collectors.toList());
        //sorted id of plans
        for (int i = 0; i < studyingPlans.size(); i++) {
            idPlansFromStudingPlans.add(studyingPlans.get(i).getId());
        }
        //sorting occupation
        occupations = occupations.stream().filter(g -> g.getNameTeacher().equals(zapros13.idTeacher)).collect(Collectors.toList());
        for (int i = 0; i < occupations.size(); i++) {
            idPlanFromOccupation.add(occupations.get(i).getIdStudingPlan());
        }

        for(int i=0;i<idPlanFromOccupation.size();i++){
            for(int j=0;j<idPlansFromStudingPlans.size();j++){
                if(idPlanFromOccupation.get(i).equals(idPlansFromStudingPlans.get(j))){
                    zapros13Outs.add(new Zapros13Out(
                            teacherService.get(zapros13.getIdTeacher()).getName(),
                            teacherService.get(zapros13.getIdTeacher()).getLastName(),
                            teacherService.get(zapros13.getIdTeacher()).getAfterFathName(),
                            subjectService.get(studyingPlanService.get(idPlansFromStudingPlans.get(j)).getOccupation()).getName(),
                            studyingPlanService.get(idPlansFromStudingPlans.get(j)).getHours()));

                }
            }
        }
        return zapros13Outs;
    }
}
