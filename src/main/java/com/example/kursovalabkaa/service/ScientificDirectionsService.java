package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.ScientificDirections;
import com.example.kursovalabkaa.repository.ScientificDirectionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ScientificDirectionsService {
    List<ScientificDirections> scientificDirections = new ArrayList<>();
    @Autowired
    ScientificDirectionsRepository scientificDirectionsRepository;

    public List<ScientificDirections> getAll() {
        return scientificDirectionsRepository.findAll();
    }

    public ScientificDirections get(String id) {
        return scientificDirectionsRepository.findById(id).orElse(null);
    }

    public ScientificDirections getOneScientificDirection(String id) {
        return scientificDirectionsRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        scientificDirectionsRepository.deleteById(id);

    }

    public void create(ScientificDirections scientificDirection) {
        scientificDirectionsRepository.save(scientificDirection);
    }

    public void update(ScientificDirections scientificDirections) {
        ScientificDirections newScientificDirection = new ScientificDirections();
        newScientificDirection.setId(scientificDirections.getId());
        newScientificDirection.setName(scientificDirections.getName());


        scientificDirectionsRepository.deleteById(scientificDirections.getId());
        scientificDirectionsRepository.save(newScientificDirection);
    }
}
