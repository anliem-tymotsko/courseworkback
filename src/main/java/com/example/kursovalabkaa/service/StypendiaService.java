package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.Stypendia;
import com.example.kursovalabkaa.repository.StypendiaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StypendiaService {
    List<Stypendia> stypendiaList = new ArrayList<>();
    @Autowired
    StypendiaRepository stypendiaRepository;

    public List<Stypendia> getAll() {
        return stypendiaRepository.findAll();
    }

    public Stypendia get(String id) {
        return stypendiaRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        stypendiaRepository.deleteById(id);

    }

    public void create(Stypendia stypendia) {
        stypendiaList.add(new Stypendia(stypendia.getName(), stypendia.getCost()));
    }

    public void update(String id, String name, int cost) {
        Stypendia newStypendia = new Stypendia();
        newStypendia.setName(name);
        newStypendia.setCost(cost);
        stypendiaRepository.deleteById(id);
        stypendiaList.add(newStypendia);
    }
}
