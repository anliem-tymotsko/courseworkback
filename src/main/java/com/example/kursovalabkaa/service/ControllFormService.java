package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.ControllForm;
import com.example.kursovalabkaa.repository.ControllFormRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ControllFormService {
    List<ControllForm> controllFormList = new ArrayList<>();
    @Autowired
    ControllFormRepository controllFormRepository;

    public List<ControllForm> getAll() {
        return controllFormRepository.findAll();
    }

    public ControllForm get(String id) {
        return controllFormRepository.findById(id).orElse(null);
    }

    public ControllForm getOneControllForm(String id) {
        return controllFormRepository.findById(id).orElse(null);
    }

    public void deleteById(String id) {
        controllFormRepository.deleteById(id);

    }

    public void create(ControllForm controllForm) {
        controllFormRepository.save(controllForm);
    }

    public void update(ControllForm controllForm) {
        ControllForm newControllForm = new ControllForm();
        newControllForm.setId(controllForm.getId());
        newControllForm.setName(controllForm.getName());
        controllFormRepository.deleteById(controllForm.getId());
        controllFormRepository.save(controllForm);
    }
}
