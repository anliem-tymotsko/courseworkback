package com.example.kursovalabkaa.service;

import com.example.kursovalabkaa.model.Diploma;
import com.example.kursovalabkaa.model.DiplomaQuery;
import com.example.kursovalabkaa.model.Zapros11Out;
import com.example.kursovalabkaa.repository.DiplomaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class Zapros11Service {
    @Autowired
    DiplomaService diplomaService;
    @Autowired
    TeacherService teacherService;
    @Autowired
    StudentService studentService;
    public List<Zapros11Out> foundByParams(DiplomaQuery diplomaQuery) {
        List<Zapros11Out> zapros11OutList = new ArrayList<>();
        List<Diploma> diplomas = diplomaService.getAll();
        List<Diploma> newdiplomas = new ArrayList<>();
        if (!diplomaQuery.getTeacher().equals(""))
            diplomas = diplomas.stream().filter(g -> g.getKerivnyk().equals(diplomaQuery.getTeacher())).collect(Collectors.toList());
        if (diplomaQuery.getKafedra().equals("")) {
            for (int i = 0; i < diplomas.size(); i++)
                if (!teacherService.getOneTeacher(diplomas.get(i).getKerivnyk()).getKafedra().equals(diplomaQuery.getKafedra()))
                    newdiplomas.add(diplomas.get(i));
        }

        for (int i = 0; i < newdiplomas.size(); i++) {
            zapros11OutList.add(new Zapros11Out(studentService.get(newdiplomas.get(i).getStudent()).getName(), studentService.get(newdiplomas.get(i).getStudent()).getLastName(), studentService.get(newdiplomas.get(i).getStudent()).getAfterFathName(), studentService.get(newdiplomas.get(i).getStudent()).getGrupa(), newdiplomas.get(i).getTheme()));

        }
        return zapros11OutList;
    }
}
