package com.example.kursovalabkaa.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Teacher {
    @Id
    private String id;
    private String name;
    private String lastName;
    private String dateOfBirth;
    private String afterFathName;
    private String specialityTeacher;
    private String kategoryT;
    private String telNum;
    private String countChildren;
    private String salary;
    private String kafedra;

    public Teacher() {
    }

    public Teacher(String name, String lastName, String afterFathName,String dateOfBirth, String specialityTeacher, String kategoryT, String telNum, String countChildren, String salary, String kafedra) {
        this.name = name;
        this.lastName = lastName;
        this.afterFathName = afterFathName;
        this.dateOfBirth = dateOfBirth;
        this.specialityTeacher = specialityTeacher;
        this.kategoryT = kategoryT;
        this.telNum = telNum;
        this.countChildren = countChildren;
        this.salary = salary;
        this.kafedra = kafedra;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAfterFathName() {
        return afterFathName;
    }

    public void setAfterFathName(String afterFathName) {
        this.afterFathName = afterFathName;
    }

    public String getSpecialityTeacher() {
        return specialityTeacher;
    }

    public void setSpecialityTeacher(String specialityTeacher) {
        this.specialityTeacher = specialityTeacher;
    }

    public String getKategoryT() {
        return kategoryT;
    }

    public void setKategoryT(String kategoryT) {
        this.kategoryT = kategoryT;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    public String getCountChildren() {
        return countChildren;
    }

    public void setCountChildren(String countChildren) {
        this.countChildren = countChildren;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getKafedra() {
        return kafedra;
    }

    public void setKafedra(String kafedra) {
        this.kafedra = kafedra;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
