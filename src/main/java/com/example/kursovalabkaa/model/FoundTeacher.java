package com.example.kursovalabkaa.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class FoundTeacher {

    private String kafedra;
    private String category;
    private String year;
    private String children;
    private String salary;
    private String age;

    public FoundTeacher() {
    }

    public FoundTeacher(String kafedra, String category, String year, String children, String salary,String age) {
        this.kafedra = kafedra;
        this.category = category;
        this.year = year;
        this.children = children;
        this.salary = salary;
        this.age = age;
    }

    public String getKafedra() {
        return kafedra;
    }

    public void setKafedra(String kafedra) {
        this.kafedra = kafedra;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
