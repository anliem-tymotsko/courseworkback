package com.example.kursovalabkaa.model;

public class Zapros5In {
    String subjectId;
    String grupaName;

    public Zapros5In() {
    }

    public Zapros5In(String subjectId, String grupaName) {
        this.subjectId = subjectId;
        this.grupaName = grupaName;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getGrupaName() {
        return grupaName;
    }

    public void setGrupaName(String grupaName) {
        this.grupaName = grupaName;
    }
}
