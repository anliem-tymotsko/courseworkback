package com.example.kursovalabkaa.model;

public class Zapros4In {
    String grupaName;
    String period;

    public Zapros4In() {
    }

    public String getGrupaName() {
        return grupaName;
    }

    public void setGrupaName(String grupaName) {
        this.grupaName = grupaName;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
