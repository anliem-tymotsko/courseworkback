package com.example.kursovalabkaa.model;

public class Zapros3Out {
    String id;
    String theme;
    String teacher;

    public Zapros3Out() {
    }

    public Zapros3Out(String id, String theme, String teacher) {
        this.theme = theme;
        this.id = id;
        this.teacher = teacher;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }
}
