package com.example.kursovalabkaa.model;

public class Zapros9Out {
    private String id;
    private String name;
    private String lastName;
    private String afterFathName;
    private String specialityTeacher;
    private String kategoryT;
    private String salary;
    private String kafedra;

    public Zapros9Out(String id, String name, String lastName, String afterFathName, String specialityTeacher, String kategoryT, String salary, String kafedra) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.afterFathName = afterFathName;
        this.specialityTeacher = specialityTeacher;
        this.kategoryT = kategoryT;
        this.salary = salary;
        this.kafedra = kafedra;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAfterFathName() {
        return afterFathName;
    }

    public void setAfterFathName(String afterFathName) {
        this.afterFathName = afterFathName;
    }

    public String getSpecialityTeacher() {
        return specialityTeacher;
    }

    public void setSpecialityTeacher(String specialityTeacher) {
        this.specialityTeacher = specialityTeacher;
    }

    public String getKategoryT() {
        return kategoryT;
    }

    public void setKategoryT(String kategoryT) {
        this.kategoryT = kategoryT;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getKafedra() {
        return kafedra;
    }

    public void setKafedra(String kafedra) {
        this.kafedra = kafedra;
    }
}
