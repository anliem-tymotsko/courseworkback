package com.example.kursovalabkaa.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class SpecialityTeacher {
    @Id
    private String id;
    private String name;

    public SpecialityTeacher() {
    }

    public SpecialityTeacher(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
