package com.example.kursovalabkaa.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Grupa {
    @Id
    private String id;
    private String name;
    private String kurator;
    private String kafedra;
    private String year;

    public Grupa() {
    }

    public Grupa(String id, String name, String kurator, String kafedra, String year) {
        this.name = name;
        this.kurator = kurator;
        this.kafedra = kafedra;
        this.year = year;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKurator() {
        return kurator;
    }

    public void setKurator(String kurator) {
        this.kurator = kurator;
    }

    public String getKafedra() {
        return kafedra;
    }

    public void setKafedra(String kafedra) {
        this.kafedra = kafedra;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
