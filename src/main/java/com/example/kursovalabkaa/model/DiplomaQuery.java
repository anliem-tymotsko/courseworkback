package com.example.kursovalabkaa.model;

public class DiplomaQuery {
    private String teacher;
    private String kafedra;

    public DiplomaQuery(String teacher, String kafedra) {
        this.teacher = teacher;
        this.kafedra = kafedra;
    }

    public DiplomaQuery() {
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getKafedra() {
        return kafedra;
    }

    public void setKafedra(String kafedra) {
        this.kafedra = kafedra;
    }
}
