package com.example.kursovalabkaa.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Occupation {
    @Id
    private String id;
    private String nameTeacher;
    private String idStudingPlan;

    public Occupation() {
    }

    public Occupation(String id, String nameTeacher, String idStudingPlan) {
        this.id = id;
        this.nameTeacher = nameTeacher;
        this.idStudingPlan = idStudingPlan;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNameTeacher() {
        return nameTeacher;
    }

    public void setNameTeacher(String nameTeacher) {
        this.nameTeacher = nameTeacher;
    }

    public String getIdStudingPlan() {
        return idStudingPlan;
    }

    public void setIdStudingPlan(String idStudingPlan) {
        this.idStudingPlan = idStudingPlan;
    }
}
