package com.example.kursovalabkaa.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class ScientistTheme {
    @Id
    private String id;
    private String themeOfWork;
    private String teacher;
    private String date;
    public ScientistTheme() {
    }

    public ScientistTheme(String themeOfWork, String teacher, String date) {
        this.themeOfWork = themeOfWork;
        this.teacher = teacher;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getThemeOfWork() {
        return themeOfWork;
    }

    public void setThemeOfWork(String themeOfWork) {
        this.themeOfWork = themeOfWork;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
