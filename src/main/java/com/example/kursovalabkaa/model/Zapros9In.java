package com.example.kursovalabkaa.model;

public class Zapros9In {
    String grupa;
    String semestr;
    String subject;

    public Zapros9In(String grupa, String semestr, String subject) {
        this.grupa = grupa;
        this.semestr = semestr;
        this.subject = subject;
    }

    public String getGrupa() {
        return grupa;
    }

    public void setGrupa(String grupa) {
        this.grupa = grupa;
    }

    public String getSemestr() {
        return semestr;
    }

    public void setSemestr(String semestr) {
        this.semestr = semestr;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
