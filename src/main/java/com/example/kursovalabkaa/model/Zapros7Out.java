package com.example.kursovalabkaa.model;

public class Zapros7Out {
    private String id;
    private String name;
    private String lastName;
    private String afterFathName;
    private String dateOfBirth;

    public Zapros7Out() {
    }

    public Zapros7Out(String id, String name, String lastName, String afterFathName, String dateOfBirth) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.afterFathName = afterFathName;
        this.dateOfBirth = dateOfBirth;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAfterFathName() {
        return afterFathName;
    }

    public void setAfterFathName(String afterFathName) {
        this.afterFathName = afterFathName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
