package com.example.kursovalabkaa.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Faculty {
    @Id
    private String id;
    private String name;
    private String kerivnyk;
    private String dekanat;

    public Faculty() {
    }

    public Faculty(String name, String kerivnyk ,String dekanat) {
        this.name = name;
        this.kerivnyk = kerivnyk;
        this.dekanat = dekanat;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKerivnyk() {
        return kerivnyk;
    }

    public void setKerivnyk(String kerivnyk) {
        this.kerivnyk = kerivnyk;
    }

    public String getDekanat() {
        return dekanat;
    }

    public void setDekanat(String dekanat) {
        this.dekanat = dekanat;
    }
}
