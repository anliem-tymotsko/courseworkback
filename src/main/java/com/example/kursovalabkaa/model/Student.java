package com.example.kursovalabkaa.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "students")
public class Student {
    @Id
    private String id;
    private String name;
    private String lastName;
    private String afterFathName;
    private String dateOfBirth;
    private String grupa;
    private String countOfChildren;
    private String stypendia;

    public Student() {
    }

    public Student(String id,String name, String lastName, String afterFathName, String dateOfBirth, String grupa, String countOfChildren, String stypendia) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.afterFathName = afterFathName;
        this.dateOfBirth = dateOfBirth;
        this.grupa = grupa;
        this.countOfChildren = countOfChildren;
        this.stypendia = stypendia;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAfterFathName() {
        return afterFathName;
    }

    public void setAfterFathName(String afterFathName) {
        this.afterFathName = afterFathName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGrupa() {
        return grupa;
    }

    public void setGrupa(String grupa) {
        this.grupa = grupa;
    }

    public String getCountOfChildren() {
        return countOfChildren;
    }

    public void setCountOfChildren(String countOfChildren) {
        this.countOfChildren = countOfChildren;
    }

    public String getStypendia() {
        return stypendia;
    }

    public void setStypendia(String stypendia) {
        this.stypendia = stypendia;
    }
}
