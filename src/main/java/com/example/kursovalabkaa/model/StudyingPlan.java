package com.example.kursovalabkaa.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class StudyingPlan {
    @Id
    private String id;
    private String grupa;
    private String univerDate;
    private String occupation;
    private String typeOccupation;
    private String hours;
    private String controllForm;

    public StudyingPlan() {
    }

    public StudyingPlan(String grupa, String univerDate, String occupation, String hours, String controllForm) {
        this.grupa = grupa;
        this.univerDate = univerDate;
        this.occupation = occupation;
        this.hours = hours;
        this.controllForm = controllForm;
    }

    public String getTypeOccupation() {
        return typeOccupation;
    }

    public void setTypeOccupation(String typeOccupation) {
        this.typeOccupation = typeOccupation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGrupa() {
        return grupa;
    }

    public void setGrupa(String grupa) {
        this.grupa = grupa;
    }

    public String getUniverDate() {
        return univerDate;
    }

    public void setUniverDate(String univerDate) {
        this.univerDate = univerDate;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getControllForm() {
        return controllForm;
    }

    public void setControllForm(String controllForm) {
        this.controllForm = controllForm;
    }
}
