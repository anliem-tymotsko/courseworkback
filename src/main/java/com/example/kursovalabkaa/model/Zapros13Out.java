package com.example.kursovalabkaa.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Zapros13Out {

    private String nameTeacher;
    private String lastNameTeacher;
    private String afterFathName;
    private String occupation;
    private String hours;

    public Zapros13Out() {
    }

    public Zapros13Out(String nameTeacher, String lastNameTeacher, String afterFathName, String occupation, String hours) {
        this.nameTeacher = nameTeacher;
        this.lastNameTeacher = lastNameTeacher;
        this.afterFathName = afterFathName;
        this.occupation = occupation;
        this.hours = hours;
    }

    public String getNameTeacher() {
        return nameTeacher;
    }

    public void setNameTeacher(String nameTeacher) {
        this.nameTeacher = nameTeacher;
    }

    public String getLastNameTeacher() {
        return lastNameTeacher;
    }

    public void setLastNameTeacher(String lastNameTeacher) {
        this.lastNameTeacher = lastNameTeacher;
    }

    public String getAfterFathName() {
        return afterFathName;
    }

    public void setAfterFathName(String afterFathName) {
        this.afterFathName = afterFathName;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }
}
