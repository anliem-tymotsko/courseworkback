package com.example.kursovalabkaa.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Kafedra {
    @Id
    private String id;
    private String name;
    private String zavKafedra;
    private String faculty;

    public Kafedra() {
    }

    public Kafedra(String id,String name, String zavKafedra, String faculty) {
        this.id = id;
        this.name = name;
        this.zavKafedra = zavKafedra;
        this.faculty = faculty;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZavKafedra() {
        return zavKafedra;
    }

    public void setZavKafedra(String zavKafedra) {
        this.zavKafedra = zavKafedra;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }
}
