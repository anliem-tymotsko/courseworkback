package com.example.kursovalabkaa.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Diploma {
    @Id
    private String id;
    private String student;
    private String kerivnyk;
    private String theme;
    private String date;

    public Diploma() {
    }

    public Diploma(String student, String kerivnyk, String theme, String date) {
        this.student = student;
        this.kerivnyk = kerivnyk;
        this.theme = theme;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getKerivnyk() {
        return kerivnyk;
    }

    public void setKerivnyk(String kerivnyk) {
        this.kerivnyk = kerivnyk;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
