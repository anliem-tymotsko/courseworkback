package com.example.kursovalabkaa.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Group {
    @Id
    private String id;
    private String name;
    private Teacher kurator;
    private Kafedra kafedra;
    private int year;

    public Group() {
    }

    public Group(String name, Teacher kurator, Kafedra kafedra, int year) {
        this.name = name;
        this.kurator = kurator;
        this.kafedra = kafedra;
        this.year = year;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Teacher getKurator() {
        return kurator;
    }

    public void setKurator(Teacher kurator) {
        this.kurator = kurator;
    }

    public Kafedra getKafedra() {
        return kafedra;
    }

    public void setKafedra(Kafedra kafedra) {
        this.kafedra = kafedra;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
