package com.example.kursovalabkaa.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Zapros13 {
    public String idStudingYear;
    public String idTeacher;

    public Zapros13() {
    }

    public Zapros13(String idStudingYear, String idTeacher) {
        this.idStudingYear = idStudingYear;
        this.idTeacher = idTeacher;
    }

    public String getIdStudingYear() {
        return idStudingYear;
    }

    public void setIdStudingYear(String idStudingYear) {
        this.idStudingYear = idStudingYear;
    }

    public String getIdTeacher() {
        return idTeacher;
    }

    public void setIdTeacher(String idTeacher) {
        this.idTeacher = idTeacher;
    }
}
