package com.example.kursovalabkaa.model;

public class Zapros7In {
    String grupa;
    String subject;
    String mark;

    public Zapros7In() {
    }

    public Zapros7In(String grupa, String subject, String mark) {
        this.grupa = grupa;
        this.subject = subject;
        this.mark = mark;
    }

    public String getGrupa() {
        return grupa;
    }

    public void setGrupa(String grupa) {
        this.grupa = grupa;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
}
