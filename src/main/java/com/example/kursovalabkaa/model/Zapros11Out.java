package com.example.kursovalabkaa.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Zapros11Out {
    @Id
    private String id;
    private String name;
    private String lastName;
    private String afterFName;
    private String grupa;
    private String themeDiplom;

    public Zapros11Out() {
    }

    public Zapros11Out(String name, String lastName, String afterFName, String grupa, String themeDiplom) {
        this.name = name;
        this.lastName = lastName;
        this.afterFName = afterFName;
        this.grupa = grupa;
        this.themeDiplom = themeDiplom;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAfterFName() {
        return afterFName;
    }

    public void setAfterFName(String afterFName) {
        this.afterFName = afterFName;
    }

    public String getGrupa() {
        return grupa;
    }

    public void setGrupa(String grupa) {
        this.grupa = grupa;
    }

    public String getThemeDiplom() {
        return themeDiplom;
    }

    public void setThemeDiplom(String themeDiplom) {
        this.themeDiplom = themeDiplom;
    }
}
