package com.example.kursovalabkaa.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Session {
    @Id
    private String id;
    private String subject;
    private String teacher;
    private String student;
    private String mark;
    private String controllForm;
    private String univerDate;

    public Session() {
    }

    public Session(String subject,String student, String teacher, String mark, String controllForm, String univerDate) {
        this.subject = subject;
        this.teacher = teacher;
        this.mark = mark;
        this.student = student;
        this.controllForm = controllForm;
        this.univerDate = univerDate;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getControllForm() {
        return controllForm;
    }

    public void setControllForm(String controllForm) {
        this.controllForm = controllForm;
    }

    public String getUniverDate() {
        return univerDate;
    }

    public void setUniverDate(String univerDate) {
        this.univerDate = univerDate;
    }
}
