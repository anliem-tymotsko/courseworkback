package com.example.kursovalabkaa.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class FoundDocentWork {

    String  teacher;
    String cafedra;

    public FoundDocentWork() {
    }

    public FoundDocentWork(String teacher, String cafedra) {
        this.teacher = teacher;
        this.cafedra = cafedra;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getCafedra() {
        return cafedra;
    }

    public void setCafedra(String cafedra) {
        this.cafedra = cafedra;
    }
}
