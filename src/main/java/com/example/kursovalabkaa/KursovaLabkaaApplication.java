package com.example.kursovalabkaa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class KursovaLabkaaApplication {

    public static void main(String[] args) {
        SpringApplication.run(KursovaLabkaaApplication.class, args);
    }

}
