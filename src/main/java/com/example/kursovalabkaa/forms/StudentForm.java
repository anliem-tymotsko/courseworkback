package com.example.kursovalabkaa.forms;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class StudentForm {
    @Id
private String id;
    private String name;
    private String lastName;
    private String afterFathName;
    private Date dateOfBirth;
    private String group;
    private int countOfChildren;
    private int stypendia;

    public StudentForm() {

    }

    public StudentForm(String name, String lastName, String afterFathName, Date dateOfBirth, String group, int countOfChildren, int stypendia) {
        this.name = name;
        this.lastName = lastName;
        this.afterFathName = afterFathName;
        this.dateOfBirth = dateOfBirth;
        this.group = group;
        this.countOfChildren = countOfChildren;
        this.stypendia = stypendia;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAfterFathName() {
        return afterFathName;
    }

    public void setAfterFathName(String afterFathName) {
        this.afterFathName = afterFathName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getCountOfChildren() {
        return countOfChildren;
    }

    public void setCountOfChildren(int countOfChildren) {
        this.countOfChildren = countOfChildren;
    }

    public int getStypendia() {
        return stypendia;
    }

    public void setStypendia(int stypendia) {
        this.stypendia = stypendia;
    }
}
