package com.example.kursovalabkaa.forms;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class DekanatForm {
    @Id
    private String id;
    private String name;
    private String dekan;

    public DekanatForm(String name, String dekan) {
        this.name = name;
        this.dekan = dekan;
    }

    public DekanatForm() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDekan() {
        return dekan;
    }

    public void setDekan(String dekan) {
        this.dekan = dekan;
    }
}
