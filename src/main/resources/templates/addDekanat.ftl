<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add dekanat</title>
</head>
<body>

<br>
<div>
    <fieldset>
        <legend>Add dekanat</legend>
        <form name="dekanat" action="" method="POST">
            Dekanat: <@spring.formInput "dekanatForm.name" "" "text"/><br/>
            Dekan: <@spring.formInput "dekanatForm.dekan" "" "text"/><br/>
            <input type="submit" value="Create"/>
        </form>
    </fieldset>
</div>
</body>
</html>