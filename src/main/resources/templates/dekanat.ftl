<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<h3>
    Dekanat list
</h3>
 <br>
<div>
    <table border="2" bgcolor="f0f8ff">
        <tr>
            <th>Name</th>
            <th>Dekan</th>
            <th>Delete</th>
        </tr>
        <#list dekanats as dekanat>
            <tr>
                <td>${dekanat.name}</td>
                <td>${dekanat.dekan}</td>
                <td><a href="/dekanat/delete/${dekanat.id}"> Delete</a> </td>
            </tr>
        </#list>
    </table>
    <a href="/dekanat/create">create</a>
    <a href="/dekanat/requestList">find</a>
</div>
</body>
</html>