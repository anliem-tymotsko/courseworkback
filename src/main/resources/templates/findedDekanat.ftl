<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<h3>
    Dekanat list
</h3>
<br>
<div>
    <table border="2" bgcolor="f0f8ff">
        <tr>
            <th>Name</th>
            <th>Dekan</th>
        </tr>
        <#list findedDekanats as dekanat>
            <tr>
                <td>${dekanat.name}</td>
                <td>${dekanat.dekan}</td>
            </tr>
        </#list>
    </table>
</div>
</body>
</html>